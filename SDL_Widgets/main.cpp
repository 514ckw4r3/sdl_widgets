#include <SDL2/SDL.h>
#include <set>

#include "WidgetContainer.h"
#include "Image.h"
#include "Button.h"
#include "Logger.h"

static const char *TITLE = "SDL_Widgets";
static const std::string IMAGES_PATH = "images/";
static const int WINDOW_WIDTH = 800;
static const int WINDOW_HEIGHT = 600;

bool InitSDL();
void InitWidgets();
void InitEventHandlers(const WidgetPtr &, const WidgetPtr &, const WidgetPtr &);
void RunEventLoop();

void SDLWindowDeleter(SDL_Window *pWindow) { SDL_DestroyWindow(pWindow); }
void SDLRendererDeleter(SDL_Renderer *pRenderer) { SDL_DestroyRenderer(pRenderer); }

std::shared_ptr<SDL_Window> g_pWindow;
std::shared_ptr<SDL_Renderer> g_pRenderer;
bool g_bRunning = true;
WidgetMultiSet g_widgets;

namespace ID
{
	static const std::string CONTAINER1 = "container1";
	static const std::string CONTAINER2 = "container2";
	static const std::string CONTAINER_BG1 = "bg1";
	static const std::string CONTAINER_BG2 = "bg2";
	static const std::string IMG1 = "img1";
	static const std::string IMG2 = "img2";
	static const std::string IMG3 = "img3";
	static const std::string BTN1 = "btn1";
	static const std::string BTN2 = "btn2";
}

////////////////////////////////////////////////////////////////////////////////

int main(int, char **)
{
	if (!InitSDL())
		return EXIT_FAILURE;

	InitWidgets();
	RunEventLoop();
	SDL_Quit();
}

////////////////////////////////////////////////////////////////////////////////

bool InitSDL()
{
	LOGD << METHOD_NAME << '\n';

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		LOGD << METHOD_NAME << " Unable to SDL_Init, error: " << SDL_GetError() << '\n';
		return false;
	}

	g_pWindow.reset(SDL_CreateWindow(TITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN), SDLWindowDeleter);
	if (!g_pWindow)
	{
		LOGD << METHOD_NAME << " Unable to SDL_CreateWindow, error: " << SDL_GetError() << '\n';
		return false;
	}

	g_pRenderer.reset(SDL_CreateRenderer(g_pWindow.get(), -1, SDL_RENDERER_ACCELERATED), SDLRendererDeleter);
	if (!g_pRenderer)
	{
		LOGD << METHOD_NAME << " Unable to SDL_CreateRenderer, error: " << SDL_GetError() << '\n';
		return false;
	}

	SDL_SetRenderDrawColor(g_pRenderer.get(), 0xFF, 0xFF, 0xFF, 0xFF);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void InitWidgets()
{
	LOGD << METHOD_NAME << '\n';

	// Container1
	WidgetContainerPtr pContainer1 = std::make_shared<WidgetContainer>();
	pContainer1->SetId(ID::CONTAINER1);
	pContainer1->SetPos(40, 40);
	pContainer1->SetSize(WINDOW_WIDTH - 80, WINDOW_HEIGHT - 80);
	g_widgets.insert(pContainer1);

	// Background1
	ImagePtr pBg1 = std::make_shared<Image>(g_pRenderer);
	pBg1->SetId(ID::CONTAINER_BG1);
	pBg1->Load(IMAGES_PATH + "bg.png");
	pBg1->SetSize(pContainer1->GetSize().first, pContainer1->GetSize().second);
	pContainer1->AddWidget(pBg1);
	// End Background1

	// Button1
	ButtonPtr pButton1 = std::make_shared<Button>(g_pRenderer);
	pButton1->SetId(ID::BTN1);
	pButton1->Load(IMAGES_PATH + "button_normal.png", IMAGES_PATH + "button_hover.png", IMAGES_PATH + "button_pressed.png");
	pButton1->SetPos(50, 50);
	pContainer1->AddWidget(pButton1);
	// End Button1

	// Image1
	ImagePtr pImage1 = std::make_shared<Image>(g_pRenderer);
	pImage1->SetId(ID::IMG1);
	pImage1->Load(IMAGES_PATH + "image1.png");
	pImage1->SetPos(330, 20);
	pImage1->SetSize(300, 300);
	pImage1->SetOrder(2);
	pContainer1->AddWidget(pImage1);
	// End Image1

	// Image2
	ImagePtr pImage2 = std::make_shared<Image>(g_pRenderer);
	pImage2->SetId(ID::IMG2);
	pImage2->Load(IMAGES_PATH + "image2.png");
	pImage2->SetPos(50, 140);
	pImage2->SetSize(300, 300);
	pImage2->SetOrder(1);
	pImage2->Hide();
	pContainer1->AddWidget(pImage2);
	// End Image2
	// End Container1

	// Container 2
	WidgetContainerPtr pWidgetContainer2 = std::make_shared<WidgetContainer>();
	pWidgetContainer2->SetId(ID::CONTAINER2);
	pWidgetContainer2->SetPos(130, 230);
	pWidgetContainer2->Hide();
	pWidgetContainer2->SetOrder(3);
	pContainer1->AddWidget(pWidgetContainer2);

	// Background2
	ImagePtr pBg2 = std::make_shared<Image>(g_pRenderer);
	pBg2->SetId(ID::CONTAINER_BG2);
	pBg2->Load(IMAGES_PATH + "bg2.png");
	pWidgetContainer2->AddWidget(pBg2);
	// End Background

	// Button2
	ButtonPtr pButton2 = std::make_shared<Button>(g_pRenderer);
	pButton2->SetId(ID::BTN2);
	pButton2->Load(IMAGES_PATH + "button_normal.png", IMAGES_PATH + "button_hover.png", IMAGES_PATH + "button_pressed.png");
	pButton2->SetOrder(1);
	pButton2->SetPos(100, 20);
	pWidgetContainer2->AddWidget(pButton2);
	// End Button2

	// Image3
	ImagePtr pImage3 = std::make_shared<Image>(g_pRenderer);
	pImage3->SetId(ID::IMG3);
	pImage3->Load(IMAGES_PATH + "image3.png");
	pImage3->SetPos(100, 100);
	pImage3->SetSize(400, 400);
	pImage3->SetOrder(2);
	pWidgetContainer2->AddWidget(pImage3);
	// End Image3
	// End Container 2

	InitEventHandlers(pImage1, pImage2, pButton1);
}

////////////////////////////////////////////////////////////////////////////////

void InitEventHandlers(const WidgetPtr &pImage1, const WidgetPtr &pImage2, const WidgetPtr &pButton1)
{
	LOGD << METHOD_NAME << '\n';

	pImage1->AddEventHandler(Widget::Event::MouseEnter, [pImage2](WidgetPtr pWidget)
	{
		LOGD << METHOD_NAME << '\n';

		pImage2->Show();
	});

	pImage1->AddEventHandler(Widget::Event::MouseLeave, [](WidgetPtr pWidget)
	{
		LOGD << METHOD_NAME << '\n';

		if (auto pContainer = pWidget->GetWidgetContainer()->Cast<WidgetContainer>())
			pContainer->GetWidget(ID::IMG2)->Hide();
	});

	pButton1->AddEventHandler(Button::Event::MouseButtonDown, [](WidgetPtr pWidget)
	{
		LOGD << METHOD_NAME << '\n';

		if (auto pContainer = pWidget->GetWidgetContainer()->Cast<WidgetContainer>())
		{
			if (auto pContainer2 = pContainer->GetWidget(ID::CONTAINER2)->Cast<WidgetContainer>())
			{
				if (pContainer2->IsVisible())
					pContainer2->Hide();
				else
					pContainer2->Show();
			}
		}
	});
}

////////////////////////////////////////////////////////////////////////////////

void RunEventLoop()
{
	LOGD << METHOD_NAME << '\n';

	SDL_Event event;
	while (g_bRunning)
	{
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_MOUSEMOTION:
				for (const WidgetPtr &pWidget : g_widgets)
					pWidget->HandleMouseMotion(event.motion.x, event.motion.y);
				break;
			case SDL_MOUSEBUTTONDOWN:
				for (const WidgetPtr &pWidget : g_widgets)
					pWidget->HandleMouseDown(event.button.x, event.button.y);
				break;
			case SDL_MOUSEBUTTONUP:
				for (const WidgetPtr &pWidget : g_widgets)
					pWidget->HandleMouseRelease(event.button.x, event.button.y);
				break;
			case SDL_QUIT:
				g_bRunning = false;
				break;
			}
		}

		SDL_RenderClear(g_pRenderer.get());

		for (const WidgetPtr &pWidget : g_widgets)
			pWidget->Render();

		SDL_RenderPresent(g_pRenderer.get());
	}
}

////////////////////////////////////////////////////////////////////////////////