#pragma once

#include <vector>
#include <set>
#include "Widget.h"

struct WidgetsComparator { bool operator()(const WidgetPtr &p1, const WidgetPtr &p2) const { return p1->GetOrder() < p2->GetOrder(); } };
typedef std::multiset<WidgetPtr, WidgetsComparator> WidgetMultiSet;

class WidgetContainer : public Widget
{
public:
	void AddWidget(WidgetPtr pWidget);
	void RemoveWidget(WidgetPtr pWidget);
	WidgetPtr GetWidget(const std::string &sId);

	virtual void Render() override;
	virtual bool HandleMouseMotion(Sint32 unX, Sint32 unY) override;
	virtual bool HandleMouseDown(Sint32 unX, Sint32 uY) override;
	virtual bool HandleMouseRelease(Sint32 unX, Sint32 uY) override;

private:
	WidgetMultiSet m_widgets;
};

typedef std::shared_ptr<WidgetContainer> WidgetContainerPtr;

