#include "Image.h"

#include <SDL2/SDL_image.h>
#include "Logger.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////

Image::Image(const RendererPtr &pRenderer)
	: m_pRenderer(pRenderer)
{
	LOGD << METHOD_NAME << '\n';
}

////////////////////////////////////////////////////////////////////////////////

bool Image::Load(const std::string &sPath)
{
	LOGD << METHOD_NAME << '\n';

	m_pTexture = Utils::LoadTexture(sPath, m_pRenderer);
	SDL_QueryTexture(m_pTexture.get(), NULL, NULL, &m_srcRect.w, &m_srcRect.h);

	return m_pTexture != nullptr;
}

////////////////////////////////////////////////////////////////////////////////

void Image::Render()
{
	if (m_bIsVisible)
	{
		Widget::Render();
		SDL_RenderCopy(m_pRenderer.get(), m_pTexture.get(), &m_srcRect, &m_dstRect);
	}
}

////////////////////////////////////////////////////////////////////////////////
