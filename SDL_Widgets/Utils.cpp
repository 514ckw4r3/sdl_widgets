#include "Utils.h"

#include <SDL2/SDL_image.h>
#include "Logger.h"

void SDLTextureDeleter(SDL_Texture *pTexture) { SDL_DestroyTexture(pTexture); }

////////////////////////////////////////////////////////////////////////////////

TexturePtr Utils::LoadTexture(const std::string &sPath, const RendererPtr &pRenderer)
{
	LOGD << METHOD_NAME << ' ' << sPath << '\n';

	TexturePtr pTexture;
	SDL_Surface *pSurface;

	if ((pSurface = IMG_Load(sPath.c_str())) == NULL)
		LOGE << METHOD_NAME << " Unable to IMG_Load, path: " << sPath << ", error: " << SDL_GetError() << '\n';
	else
	{
		pTexture.reset(SDL_CreateTextureFromSurface(pRenderer.get(), pSurface), SDLTextureDeleter);
		if (!pTexture)
			LOGE << METHOD_NAME << " Unable to CreateTextureFromSurface path: " << sPath << ", error: " << SDL_GetError() << '\n';
		SDL_FreeSurface(pSurface);
	}

	return pTexture;
}

////////////////////////////////////////////////////////////////////////////////
