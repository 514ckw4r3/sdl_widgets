#pragma once

#include <string>
#include <functional>
#include <memory>
#include <utility>
#include <SDL2/SDL_stdinc.h>

class IWidget;
typedef std::function<void(std::shared_ptr<IWidget>)> WidgetEventHandler;

class IWidget
{
public:
	enum class Event
	{
		MouseButtonDown,
		MouseButtonRelease ,
		MouseEnter,
		MouseLeave
	};

	virtual ~IWidget() {}

	virtual void SetId(const std::string &sId) = 0;
	virtual void SetOrder(int nOrder) = 0;
	virtual void SetPos(int nX, int nY) = 0;
	virtual void SetSize(int nWidth, int nHeight) = 0;

	virtual std::string GetId() const = 0;
	virtual int GetOrder() const = 0;
	virtual std::pair<int, int> GetPos() const = 0;
	virtual std::pair<int, int> GetSize() const = 0;

	virtual void OnMouseButtonDown() = 0;
	virtual void OnMouseButtonRelease() = 0;
	virtual void OnMouseEnter() = 0;
	virtual void OnMouseLeave() = 0;

	virtual void Render() = 0;
	virtual void Show() = 0;
	virtual void Hide() = 0;
	virtual bool IsVisible() const = 0;

	virtual bool HandleMouseMotion(Sint32 unX, Sint32 unY) = 0;
	virtual bool HandleMouseDown(Sint32 unX, Sint32 unY) = 0;
	virtual bool HandleMouseRelease(Sint32 unX, Sint32 unY) = 0;
	virtual void ClearHoverState() = 0;
	virtual void AddEventHandler(Event event, const WidgetEventHandler &eventHandler) = 0;

	virtual void SetWidgetContainer(const std::shared_ptr<IWidget> &pParentContainer) = 0;
	virtual std::shared_ptr<IWidget> GetWidgetContainer() = 0;

	template<typename T>
	T * Cast()
	{
		if (auto ptr = dynamic_cast<T *>(this))
			return ptr;
		return nullptr;
	}

	explicit IWidget() = default;
	IWidget(const IWidget &) = delete;
	IWidget &operator=(const IWidget &) = delete;
};

typedef std::shared_ptr<IWidget> WidgetPtr;
