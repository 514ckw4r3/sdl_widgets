#include "WidgetContainer.h"

#include "Logger.h"
#include <SDL2/SDL.h>

////////////////////////////////////////////////////////////////////////////////

void WidgetContainer::AddWidget(WidgetPtr pWidget)
{
	LOGD << METHOD_NAME;

	m_widgets.insert(pWidget);
	pWidget->SetWidgetContainer(shared_from_this());
}

////////////////////////////////////////////////////////////////////////////////

void WidgetContainer::RemoveWidget(WidgetPtr pWidget)
{
	LOGD << METHOD_NAME << " id: " << pWidget->GetId() << '\n';
	m_widgets.erase(pWidget);
}

////////////////////////////////////////////////////////////////////////////////

WidgetPtr WidgetContainer::GetWidget(const std::string &sId)
{
	LOGD << METHOD_NAME << " id: " << sId << '\n';

	auto it = std::find_if(m_widgets.cbegin(), m_widgets.cend(), [&sId](const WidgetPtr &p1) { return p1->GetId() == sId; });
	if (it == m_widgets.end())
	{
		LOGE << METHOD_NAME << " Unable to find: " << sId << '\n';
		return nullptr;
	}
	else
		return *it;
}

////////////////////////////////////////////////////////////////////////////////

void WidgetContainer::Render()
{
	if (!m_bIsVisible)
		return;

	Widget::Render();

	for (const auto &pWidget : m_widgets)
		pWidget->Render();
}

////////////////////////////////////////////////////////////////////////////////

bool WidgetContainer::HandleMouseMotion(Sint32 unX, Sint32 unY)
{
	if (!m_bIsVisible)
		return false;

	bool bHandled = false;
	for (auto it = m_widgets.rbegin(); it != m_widgets.rend(); ++it)
	{
		if (!bHandled)
		{
			if ((*it)->HandleMouseMotion(unX, unY))
				bHandled = true;
		}
		else
			(*it)->ClearHoverState();
	}
	return bHandled;
}

////////////////////////////////////////////////////////////////////////////////

bool WidgetContainer::HandleMouseDown(Sint32 unX, Sint32 unY)
{
	if (!m_bIsVisible)
		return false;

	for (auto it = m_widgets.rbegin(); it != m_widgets.rend(); ++it)
		if ((*it)->HandleMouseDown(unX, unY))
			return true;
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool WidgetContainer::HandleMouseRelease(Sint32 unX, Sint32 unY)
{
	if (!m_bIsVisible)
		return false;

	for (auto it = m_widgets.rbegin(); it != m_widgets.rend(); ++it)
		if ((*it)->HandleMouseRelease(unX, unY))
			return true;
	return false;
}

////////////////////////////////////////////////////////////////////////////////
