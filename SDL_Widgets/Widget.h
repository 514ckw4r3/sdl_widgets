#pragma once

#include <SDL2/SDL_rect.h>
#include <unordered_map>
#include "IWidget.h"

class Widget : public IWidget, public std::enable_shared_from_this<Widget>
{
public:
	virtual void SetId(const std::string &sId) override;
	virtual void SetOrder(int nOrder) override;
	virtual void SetPos(int nX, int nY) override;
	virtual void SetSize(int nWidth, int nHeight) override;

	virtual std::string GetId() const override;
	virtual int GetOrder() const override;
	virtual std::pair<int, int> GetPos() const override;
	virtual std::pair<int, int> GetSize() const override;

	virtual void OnMouseButtonDown() override;
	virtual void OnMouseButtonRelease() override;
	virtual void OnMouseEnter() override;
	virtual void OnMouseLeave() override;

	virtual void Render() override;
	virtual void Show() override;
	virtual void Hide() override;
	virtual bool IsVisible() const override;

	virtual bool HandleMouseMotion(Sint32 unX, Sint32 unY) override;
	virtual bool HandleMouseDown(Sint32 unX, Sint32 unY) override;
	virtual bool HandleMouseRelease(Sint32 unX, Sint32 unY) override;
	virtual void ClearHoverState() override;
	virtual void AddEventHandler(Event event, const WidgetEventHandler &eventHandler) override;

	virtual void SetWidgetContainer(const WidgetPtr &pWidgetContainer) override;
	virtual WidgetPtr GetWidgetContainer() override;

protected:
	void CallEventHandler(Event event);

	std::string m_sId;
	int m_nOrder = 0;

	bool m_bIsVisible = true;
	bool m_bIsMouseEnter = false;
	bool m_bIsMouseDown = false;

	SDL_Rect m_rect = { 0 };
	SDL_Rect m_srcRect = { 0 };
	SDL_Rect m_dstRect = { 0 };

	WidgetPtr m_pWidgetContainer;

	std::unordered_map<Event, WidgetEventHandler> m_eventHandlers;
};

