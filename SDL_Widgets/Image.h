#pragma once

#include "Widget.h"

struct SDL_Texture;
struct SDL_Renderer;

typedef std::shared_ptr<SDL_Renderer> RendererPtr;
typedef std::shared_ptr<SDL_Texture> TexturePtr;

class Image : public Widget
{
public:
	explicit Image(const RendererPtr &pRenderer);

	bool Load(const std::string &sPath);
	virtual void Render() override;

private:
	TexturePtr m_pTexture;
	RendererPtr m_pRenderer;
};

typedef std::shared_ptr<Image> ImagePtr;

