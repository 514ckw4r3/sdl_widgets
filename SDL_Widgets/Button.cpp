#include "Button.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Logger.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////

Button::Button(const RendererPtr &pRenderer)
	: m_pRenderer(pRenderer)
{
	LOGD << METHOD_NAME << '\n';
}

////////////////////////////////////////////////////////////////////////////////

bool Button::Load(const std::string &sNormalImagePath, const std::string &sHoverImagePath, const std::string &sMouseDownImagePath)
{
	LOGD << METHOD_NAME << '\n';

	m_pNormalTexture = Utils::LoadTexture(sNormalImagePath, m_pRenderer);
	m_pHoverTexture = Utils::LoadTexture(sHoverImagePath, m_pRenderer);
	m_pMouseDownTexture = Utils::LoadTexture(sMouseDownImagePath, m_pRenderer);
	SDL_QueryTexture(m_pNormalTexture.get(), NULL, NULL, &m_srcRect.w, &m_srcRect.h);

	return m_pNormalTexture && m_pHoverTexture && m_pMouseDownTexture;
}

////////////////////////////////////////////////////////////////////////////////

void Button::Render()
{
	if (m_bIsVisible)
	{
		Widget::Render();

		SDL_Texture *pActiveTexture;
		if (m_bIsMouseDown)
			pActiveTexture = m_pMouseDownTexture.get();
		else
			pActiveTexture = m_bIsMouseEnter ? m_pHoverTexture.get() : m_pNormalTexture.get();
		SDL_RenderCopy(m_pRenderer.get(), pActiveTexture, &m_srcRect, &m_dstRect);
	}
}

////////////////////////////////////////////////////////////////////////////////
