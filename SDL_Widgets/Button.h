#pragma once

#include "Widget.h"

struct SDL_Texture;
struct SDL_Renderer;

typedef std::shared_ptr<SDL_Renderer> RendererPtr;
typedef std::shared_ptr<SDL_Texture> TexturePtr;

class Button : public Widget
{
public:
	explicit Button() = default;
	explicit Button(const RendererPtr &pRenderer);

	bool Load(const std::string &sNormalImagePath, const std::string &sHoverImagePath, const std::string &sMouseDownImagePath);
	virtual void Render() override;

private:
	TexturePtr m_pNormalTexture;
	TexturePtr m_pHoverTexture;
	TexturePtr m_pMouseDownTexture;
	RendererPtr m_pRenderer;
};

typedef std::shared_ptr<Button> ButtonPtr;
