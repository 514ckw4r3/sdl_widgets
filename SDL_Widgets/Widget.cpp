#include "Widget.h"

#include "Logger.h"

////////////////////////////////////////////////////////////////////////////////

void Widget::SetId(const std::string &sId)
{
	LOGD << METHOD_NAME << " id: " << sId << '\n';
	m_sId = sId;
}

////////////////////////////////////////////////////////////////////////////////

void Widget::SetOrder(int nOrder)
{
	LOGD << METHOD_NAME << " order: " << nOrder << '\n';
	m_nOrder = nOrder;
}

////////////////////////////////////////////////////////////////////////////////

void Widget::SetPos(int nX, int nY)
{
	LOGD << METHOD_NAME << " pos: " << nX << ':' << nY << '\n';
	m_rect.x = nX;
	m_rect.y = nY;
}

////////////////////////////////////////////////////////////////////////////////

void Widget::SetSize(int nWidth, int nHeight)
{
	LOGD << METHOD_NAME << " size: " << nWidth << ':' << nHeight << '\n';
	m_rect.w = nWidth;
	m_rect.h = nHeight;
}

////////////////////////////////////////////////////////////////////////////////

std::string Widget::GetId() const
{
	return m_sId;
}

////////////////////////////////////////////////////////////////////////////////

int Widget::GetOrder() const
{
	return m_nOrder;
}

////////////////////////////////////////////////////////////////////////////////

std::pair<int, int> Widget::GetPos() const
{
	return { m_rect.x, m_rect.y };
}

////////////////////////////////////////////////////////////////////////////////

std::pair<int, int> Widget::GetSize() const
{
	return { m_rect.w, m_rect.h };
}

////////////////////////////////////////////////////////////////////////////////

void Widget::OnMouseButtonDown()
{
	LOGD << METHOD_NAME << " id: " << m_sId << '\n';

	CallEventHandler(Event::MouseButtonDown);
}

////////////////////////////////////////////////////////////////////////////////

void Widget::OnMouseButtonRelease()
{
	LOGD << METHOD_NAME << " id: " << m_sId << '\n';

	CallEventHandler(Event::MouseButtonRelease);
}

////////////////////////////////////////////////////////////////////////////////

void Widget::OnMouseEnter()
{
	LOGD << METHOD_NAME << " id: " << m_sId << '\n';

	CallEventHandler(Event::MouseEnter);
}

////////////////////////////////////////////////////////////////////////////////

void Widget::OnMouseLeave()
{
	LOGD << METHOD_NAME << " id: " << m_sId << '\n';

	CallEventHandler(Event::MouseLeave);
}

////////////////////////////////////////////////////////////////////////////////

void Widget::Render()
{
	if (!m_bIsVisible)
		return;

	if (!m_pWidgetContainer)
		m_dstRect = m_rect;
	else if (auto pContainer = m_pWidgetContainer->Cast<Widget>())
	{
		m_dstRect = { m_rect.x, m_rect.y, m_rect.w ? m_rect.w : m_srcRect.w, m_rect.h ? m_rect.h : m_srcRect.h };

		// Initialization of the size of the nested container, if not specified
		if (!m_dstRect.w && pContainer->m_rect.w)
			m_dstRect.w = pContainer->m_dstRect.w - m_dstRect.x;
		if (!m_dstRect.h && pContainer->m_rect.h)
			m_dstRect.h = pContainer->m_dstRect.h - m_dstRect.y;

		// Checking if the widget goes beyond the container
		if (m_dstRect.x < 0)
		{
			m_dstRect.w -= std::abs(m_dstRect.x);
			m_srcRect.x = std::abs(m_dstRect.x);
			m_dstRect.x = 0;
		}
		if (m_dstRect.y < 0)
		{
			m_dstRect.h -= std::abs(m_dstRect.y);
			m_srcRect.y = std::abs(m_dstRect.y);
			m_dstRect.y = 0;
		}
		if (pContainer->m_dstRect.w && (m_dstRect.x + m_dstRect.w > pContainer->m_dstRect.w))
		{
			m_dstRect.w = pContainer->m_dstRect.w - m_dstRect.x;
			m_srcRect.w = m_dstRect.w;
		}
		if (pContainer->m_dstRect.h && (m_dstRect.y + m_dstRect.h > pContainer->m_dstRect.h))
		{
			m_dstRect.h = pContainer->m_dstRect.h - m_dstRect.y;
			m_srcRect.h = m_dstRect.h;
		}

		m_dstRect.x += pContainer->m_dstRect.x;
		m_dstRect.y += pContainer->m_dstRect.y;
	}
}

////////////////////////////////////////////////////////////////////////////////

void Widget::Show()
{
	LOGD << METHOD_NAME << '\n';
	m_bIsVisible = true;
}

////////////////////////////////////////////////////////////////////////////////

void Widget::Hide()
{
	LOGD << METHOD_NAME << '\n';
	m_bIsVisible = false;
}

////////////////////////////////////////////////////////////////////////////////

bool Widget::IsVisible() const
{
	return m_bIsVisible;
}

////////////////////////////////////////////////////////////////////////////////

bool Widget::HandleMouseMotion(Sint32 unX, Sint32 unY)
{
	if (!m_bIsVisible)
		return false;

	bool bPointInXAxis = m_dstRect.x < unX && m_dstRect.x + m_dstRect.w > unX;
	bool bPointInYAxis = m_dstRect.y < unY && m_dstRect.y + m_dstRect.h > unY;

	if (bPointInXAxis && bPointInYAxis)
	{
		if (!m_bIsMouseEnter)
		{
			m_bIsMouseEnter = true;
			OnMouseEnter();
		}
	}
	else
	{
		if (m_bIsMouseEnter)
		{
			m_bIsMouseEnter = false;
			OnMouseLeave();
		}
	}
	return m_bIsMouseEnter;
}

////////////////////////////////////////////////////////////////////////////////

bool Widget::HandleMouseDown(Sint32 unX, Sint32 unY)
{
	if (!m_bIsVisible)
		return false;

	bool bPointInXAxis = m_dstRect.x < unX && m_dstRect.x + m_dstRect.w > unX;
	bool bPointInYAxis = m_dstRect.y < unY && m_dstRect.y + m_dstRect.h > unY;

	if (bPointInXAxis && bPointInYAxis)
	{
		m_bIsMouseDown = true;
		OnMouseButtonDown();
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool Widget::HandleMouseRelease(Sint32 unX, Sint32 unY)
{
	if (!m_bIsVisible)
		return false;

	if (m_bIsMouseDown)
	{
		m_bIsMouseDown = false;
		OnMouseButtonRelease();
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void Widget::ClearHoverState()
{
	if (m_bIsMouseEnter)
	{
		m_bIsMouseEnter = false;
		OnMouseLeave();
	}
}

////////////////////////////////////////////////////////////////////////////////

void Widget::AddEventHandler(Event event, const WidgetEventHandler &eventHandler)
{
	LOGD << METHOD_NAME << '\n';
	m_eventHandlers[event] = eventHandler;
}

////////////////////////////////////////////////////////////////////////////////

void Widget::SetWidgetContainer(const WidgetPtr &pWidgetContainer)
{
	m_pWidgetContainer = pWidgetContainer;
}

////////////////////////////////////////////////////////////////////////////////

WidgetPtr Widget::GetWidgetContainer()
{
	return m_pWidgetContainer;
}

////////////////////////////////////////////////////////////////////////////////

void Widget::CallEventHandler(Event event)
{
	LOGD << METHOD_NAME << '\n';

	auto it = m_eventHandlers.find(event);
	if (it != m_eventHandlers.end())
		it->second(shared_from_this());
}

////////////////////////////////////////////////////////////////////////////////
