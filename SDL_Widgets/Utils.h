#pragma once

#include <string>
#include <memory>

struct SDL_Texture;
struct SDL_Renderer;

typedef std::shared_ptr<SDL_Renderer> RendererPtr;
typedef std::shared_ptr<SDL_Texture> TexturePtr;

namespace Utils
{
	TexturePtr LoadTexture(const std::string &sPath, const RendererPtr &pRenderer);
};
